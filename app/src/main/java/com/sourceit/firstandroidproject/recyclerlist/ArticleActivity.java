package com.sourceit.firstandroidproject.recyclerlist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.sourceit.firstandroidproject.R;
import com.sourceit.firstandroidproject.list.Article;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticleActivity extends AppCompatActivity {

    public static final String EXTRA_ARTICLE = "article";

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.text)
    TextView text;
    Article article;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        ButterKnife.bind(this);
        if (getIntent().hasExtra(EXTRA_ARTICLE)) {
            article = (Article) getIntent().getExtras().getSerializable(EXTRA_ARTICLE);
            if (article != null) {
                title.setText(article.getTitle());
                text.setText(article.getText());
            }
        }

    }
}
