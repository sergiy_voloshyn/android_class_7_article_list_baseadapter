package com.sourceit.firstandroidproject.list;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by wenceslaus on 16.01.18.
 */

public class Article implements Parcelable {

    private String title;
    private String text;
    private int year;
    private long time;


    public Article(String title, String text) {
        this.title = title;
        this.text = text;
    }

    protected Article(Parcel in) {
        title = in.readString();
        text = in.readString();
        year = in.readInt();
        time = in.readLong();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(text);
        dest.writeInt(year);
        dest.writeLong(time);
    }
}

