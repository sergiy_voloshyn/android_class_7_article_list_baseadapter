package com.sourceit.firstandroidproject.landscape;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sourceit.firstandroidproject.R;
import com.sourceit.firstandroidproject.list.Article;
import com.sourceit.firstandroidproject.list.Generator;

import java.util.List;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class MultipleOrientationActivity extends AppCompatActivity implements ElementClickListener {

    boolean isPortrait;

    ListFragment listFragment;
    ItemFragment itemFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_orientation);
        isPortrait = getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT;

        List<Article> articles = Generator.generateList();

        listFragment = ListFragment.newInstance();
        listFragment.setList(articles);
        itemFragment = ItemFragment.newInstance();
        itemFragment.updateArticle(articles.get(0));

        if (isPortrait) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.base_container, listFragment)
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.left_container, listFragment)
                    .replace(R.id.right_container, itemFragment)
                    .commit();
        }
    }

    @Override
    public void onItemClick(Article article) {

        itemFragment.updateArticle(article);

        if (isPortrait) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.base_container, itemFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

}
