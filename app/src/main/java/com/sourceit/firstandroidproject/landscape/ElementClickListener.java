package com.sourceit.firstandroidproject.landscape;

import com.sourceit.firstandroidproject.list.Article;

/**
 * Created by wenceslaus on 23.01.18.
 */

public interface ElementClickListener {
    void onItemClick(Article article);
}
